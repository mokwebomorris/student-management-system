package com.mainstream.studentmanagementsystem.controller;

import com.mainstream.studentmanagementsystem.entity.Student;
import com.mainstream.studentmanagementsystem.service.StudentService;
import com.mainstream.studentmanagementsystem.serviceImpl.StudentServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class StudentController {

    private StudentService studentService;

    @Autowired
    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    /**
     * handler method to handle list of all students and return mode and view
     * @param model
     * @return
     */
    @GetMapping("/students")
    public String listStudents(Model model){
        model.addAttribute("students", studentService.getAllStudents());
        return  "students";
    }



    @GetMapping("/students/new")
    public String createStudentForm(Model model){

        //create student object to hold student date
        Student student = new Student();
        model.addAttribute("student",student);
        return "create_student";
    }

    /**
     * handler method to handle new student creation
     * @param student
     * @return
     */
    @PostMapping("/newstudent")
    public String saveStudents(@ModelAttribute("student") Student student){
        studentService.saveStudent(student);
        return "redirect:/students";
    }

    @GetMapping("/students/edit/{id}")
    public String editStudentForm(@PathVariable Long id, Model model){
        model.addAttribute("student", studentService.getStudentById(id));
        return "edit_student";
    }

    /**
     * handler method to handle updating existing students
     */
    @PostMapping("/students/{id}")
    public String updateStudent( @PathVariable Long id,@ModelAttribute("student") Student student, Model model){

        //get existing student
        Student existingStudent = studentService.getStudentById(id);
        existingStudent.setId(student.getId());
        existingStudent.setFirstName(student.getFirstName());
        existingStudent.setLastName(student.getLastName());
        existingStudent.setEmail(student.getEmail());

        //save the update
        studentService.updateStudent(existingStudent);
        return  "redirect:/students";
    }

    /**
     * handler method to handle delete student
     */
    @GetMapping("/students/delete/{id}")
    public String deleteStudent(@PathVariable Long id){
        studentService.deleteStudentById(id);
        return "redirect:/students";
    }

}
